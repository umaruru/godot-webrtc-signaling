# GWS - Godot WebRTC Signaling

This project aims to facilitate using WebRTC in Godot Engine. It provides a signaling server and several functions to make the connection process easier.