# Messages

## Signaling Messages

### C > S :: Client to Server

---
**create_room**

client requests to create a room

	{
	    "type": "create_room"
	}

---
**remove_room**

client requests to remove their room

    {
	    "type": "remove_room"
    }

---
**join_room**

client requests to join a room

|Param|type|description|
|--|--|--|
| room_id | string | the id of the room to be joined |

    {
	    "type": "join_room",
	    "room_id": "0A1B2C3D"
    }

---
**join_accept**

the host accepts a join request from a client.

|Param|type|description|
|--|--|--|
| sig_client_id | int | the id of the accepted client |

    {
	    "type": "join_accept",
	    "sig_client_id": 567890
    }

---
**join_refuse**

the host refuses a join request from a client.

|Param|type|description|
|--|--|--|
| sig_client_id | int | the id of refused client |
| reason | string | the reason a client can't join |

	{
	    "type": "join_refuse",
	    "sig_client_id": 456123,
	    "reason": "The room is full"
	}

---
**send_offer**

a host is sending a WebRTC offer to a client.

|Param|type|description|
|--|--|--|
| sig_client_id | int | the signaling client receiver of the offer |
| from_peer | int | the ID of the WebRTCPeer that created the offer |
| to_peer | int | the destination peer id (client will create a WebRTCPeerConnection with this ID) |
| offer | object | the offer object created by host's WebRTCPeerConnection |

    {
	    "type": "send_offer",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"offer": { offer object }
    }

---
**send_answer**

a client is sending a WebRTC answer to a host

|Param|type|description|
|--|--|--|
| sig_client_id | int | the signaling client receiver of the answer |
| from_peer_id | int | id of the WebRTCPeer that created the answer |
| to_peer_id | int |id of the peer this answer is destinated to |
| answer | object | the answer object created by WebRTCPeer |

    {
	    "type": "send_answer",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"answer": { answer object }
    }

---
**send_candidate**

a peer is sending an ICE candidate to another peer

|Param|type|description|
|--|--|--|
| sig_client_id | int | the signaling client receiver of the candidate |
| from_peer_id | int | id of the WebRTCPeer is sending the candidate |
| to_peer_id | int |the peer this candidate is destinated to |
| candidate | object | the candidate object |

    {
	    "type": "send_candidate",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"candidate": { candidate object }
    }

---
**send_signal**

a peer is sending an signaling information to another peer

|Param|type|description|
|--|--|--|
| signal_type | string | the type of the signal. can be an _offer_, an _answer_, or a _candidate_ |
| sig_client_id | int | the id of the signaling client this info is being sent to |
| from_peer_id | int | id of the WebRTCPeer that created the info |
| to_peer_id | int |the peer this info is destinated to |
| object | object | the information object |

    {
	    "type": "send_signal",
	    "signal_type": "offer",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"object": { offer object }
    }

### S > C :: Server to Client

---
**room_created**

room was created for the client. The ID can be sent to other players to join this room

|Param|type|description|
|--|--|--|
| room_id | string | the id of the newly created room |

	{
	    "type": "room_created",
	    "room_id": "A0B1C2D3"
	}

---
**room_create_error**

server could not create a room.

|Param|type|description|
|--|--|--|
| error | string | the reason why the room wasn't created |

	{
	    "type": "room_create_error",
	    "error": "Room wasn't created because reasons"
	}

---
**join_request**

sends a join request to from a client to a host

|Param|type|description|
|--|--|--|
| sig_client_id | int | the id of of the client requesting to join |

	{
	    "type": "join_request",
	    "sig_client_id": 654321
	}

---
**join_request_accepted**

a client's join request was accepted by the host

|Param|type|description|
|--|--|--|
| room_id | string | the id of the room |
| sig_client_id | int | id of the host of the room |


	{
	    "type": "join_request",
	    "sig_client_id": 654321
	}

---
**join_request_refused**

a client's join request was refused by the host

|Param|type|description|
|--|--|--|
| room_id | string | the id of the room |
| reason | string | reason why the join request was refused |


	{
	    "type": "join_request_refused",
	    "reason": "The room is full!"
	}

---
**offer_received**

a host sent an offer to the client

|Param|type|description|
|--|--|--|
| sig_client_id | int | the id of the signaling client that sent the offer (host id) |
| from_peer_id | int | the ID of the WebRTCPeer that created the offer |
| to_peer_id | int | the destination peer id (the client will create a WebRTCPeerConnection with this ID) |
| offer | object | the offer object created by the host |

    {
	    "type": "offer_received",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"offer": { offer object }
    }

---
**answer_received**

a host sent an offer to the client

|Param|type|description|
|--|--|--|
| sig_client_id | int | the id of the signaling client that sent the answer (client id) |
| from_peer_id | int | the ID of the WebRTCPeer that created the answer |
| to_peer_id | int | the destination peer id (the client will create a WebRTCPeerConnection with this ID) |
| answer | object | the offer object created by the host |

    {
	    "type": "offer_received",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"answer": { offer object }
    }

---
**candidate_received**

a peer sent a candidate to another peer

|Param|type|description|
|--|--|--|
| sig_client_id | int | the id of the signaling client that sent the candidate (client id) |
| from_peer_id | int | the ID of the WebRTCPeer that created the candidate |
| to_peer_id | int | the destination peer id of this candidate |
| candidate | object | the offer object created by the host |

    {
	    "type": "candidate_received",
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"candidate": { candidate object }
    }

---
**signal_received**

a peer has sent a signaling information  to another peer

|Param|type|description|
|--|--|--|
| signal_type | string | the type of the signal. can be an _offer_, an _answer_, or a _candidate_
| sig_client_id | int | the id of the signaling client that sent the information |
| from_peer_id | int | the ID of the WebRTCPeer that created the information |
| to_peer_id | int | the destination peer id (the client will create a WebRTCPeerConnection with this ID) |
| object | object | the information object |

    {
	    "type": "signal_received",
	    "signal_type": "offer"
		"sig_client_id": 123654,
		"from_peer_id": 654123,
		"to_peer_id": 789012,
		"object": { offer object }
    }
